#ifndef ROBOTVIDEODISPLAY_H
#define ROBOTVIDEODISPLAY_H

#include <Core/System/Network/FlowNetwork/skflowsat.h>
#include <Core/System/Network/FlowNetwork/skflowvideosubscriber.h>

#include <Multimedia/Image/skimageutils.h>
#include <Multimedia/Image/skimagedecoder.h>

#include <UI/FLTK/skfltkui.h>
#include <UI/FLTK/skfltkwindows.h>
#include <UI/FLTK/skfltkhlayout.h>
#include <UI/FLTK/skfltkbuttons.h>
#include <UI/FLTK/skfltkmenus.h>
#include <UI/FLTK/skfltkviews.h>

class RobotVideoDisplay extends SkFlowSat
{
    SkFlowVideoSubscriber *subscriber;

    SkTreeMap<SkString, SkVariant> guiIndex;

    SkFltkDoubleWindow *window;
    SkFltkImageBox *videoWidget;
    SkFltkComboBox *chansCombo;

    public:
        Constructor(RobotVideoDisplay, SkFlowSat);

        Slot(fullScreen);
        Slot(selectChannel);

    private:
        void buildUI();

        bool onSetup()                                  override;
        void onInit()                                   override;
        void onQuit()                                   override;

        void onChannelAdded(SkFlowChanID chanID)        override;
        void onChannelRemoved(SkFlowChanID chanID)      override;

        //void onFlowDataCome(SkFlowChannelData &chData)  override;
};

#endif // ROBOTVIDEODISPLAY_H
