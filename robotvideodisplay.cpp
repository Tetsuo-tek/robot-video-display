#include "robotvideodisplay.h"
#include "gui.h"

#include <Core/Containers/skarraycast.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(RobotVideoDisplay, SkFlowSat)
{
    window = nullptr;
    videoWidget = nullptr;
    chansCombo = nullptr;

    subscriber = nullptr;

    setObjectName("RobotVideo");

    SlotSet(fullScreen);
    SlotSet(selectChannel);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotVideoDisplay::buildUI()
{
    SkVariant indexVal;
    indexVal.fromJson(GUI_INDEX);
    indexVal.copyToMap(guiIndex);

    int index = guiIndex["gui/mainwindow.json"].toInt();
    window = DynCast(SkFltkDoubleWindow, ui->loadFromJSON(gui[index].data));
    Attach(window, closed, this, quit, SkQueued);

    videoWidget = DynCast(SkFltkImageBox, ui->get("videoWidget"));

    SkFltkLightButton *fsButton = DynCast(SkFltkLightButton, ui->get("fsButton"));
    Attach(fsButton, clicked, this, fullScreen, SkQueued);

    chansCombo = DynCast(SkFltkComboBox, ui->get("chansCombo"));
    Attach(chansCombo, picked, this, selectChannel, SkQueued);

    SkFltkButton *quitButton = DynCast(SkFltkButton, ui->get("quitButton"));
    Attach(quitButton, clicked, this, quit, SkQueued);

    window->show();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotVideoDisplay::onSetup()
{
    subscriber = new SkFlowVideoSubscriber(this);
    setupSubscriber(subscriber);

    return true;
}

void RobotVideoDisplay::onInit()
{
    buildUI();
}

void RobotVideoDisplay::onQuit()
{
    if (subscriber)
        subscriber->close();

    window = nullptr;
    videoWidget = nullptr;
    chansCombo = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotVideoDisplay::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);

    if (!chansCombo)
        return;

    if (ch->flow_t == FT_VIDEO_DATA || ch->flow_t == FT_VIDEO_PREVIEW_DATA || ch->flow_t == FT_CV_OBJECT_DETECTED_FRAME)
    {
        /*int index = */chansCombo->add(ch->name.c_str());

        /*if (index > -1 && SkString::compare(ch->name.c_str(), subscriber->chanName()))//ch->name == inputChanName)
            chansCombo->setCurrentIndex(index);*/
    }
}

void RobotVideoDisplay::onChannelRemoved(SkFlowChanID chanID)
{
    if (!chansCombo)
        return;

    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);

    int index = chansCombo->find(ch->name.c_str());

    if (index > -1)
        chansCombo->remove(index);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*void RobotVideoDisplay::onFlowDataCome(SkFlowChannelData &chData)
{
    if (!videoWidget || chData.data.isEmpty())
        return;

    subscriber->tick(chData);
}*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotVideoDisplay, fullScreen)
{
    SilentSlotArgsWarning();

    static bool fullscreen = false;

    if (!fullscreen)
        window->enableFullScreen();

    else
        window->disableFullScreen();

    window->redraw();
    fullscreen = !fullscreen;
}

SlotImpl(RobotVideoDisplay, selectChannel)
{
    SilentSlotArgsWarning();

    SkFlowChannel *ch = channel(chansCombo->currentText());
    AssertKiller(!ch);

    if (subscriber->isReady())
        subscriber->unsubscribe();

    subscriber->setup(videoWidget);
    subscriber->subscribe(ch->name.c_str());
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
